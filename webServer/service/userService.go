package service

import (
	"errors"
	"github.com/fatih/pool"
	"log"
	"webServer/model"
	"webServer/myRpc"
)

var userPool *pool.Pool

func init() {

}

func CheckLogin(username string, token string) (bool, error) {
	//连接rpc
	checked, errMsg := myRpc.CheckLogin(username, token)
	if errMsg != "" || checked == false {
		log.Println("TokenCheck error: ", errMsg)
		return checked, errors.New(errMsg)
	}
	return checked, nil
}

func Login(username string, password string) (token string, err error) {
	//连接rpc
	token, errMsg := myRpc.Login(username, password)

	if errMsg != "" {
		log.Println("Login error: ", errMsg)
		return "", errors.New(errMsg)
	}
	return token, nil
}

func Logout(username string, token string) error {
	errMsg := myRpc.Logout(username, token)

	if errMsg != "" {
		log.Println("logout error: ", errMsg)
		return errors.New(errMsg)
	}
	return nil
}

func GetUserData(username string) (model.UserData, error) {
	userData, errMsg := myRpc.GetUserData(username)

	if errMsg != "" {
		log.Println("get user data error: ", errMsg)
		return model.UserData{}, errors.New(errMsg)
	}
	return userData, nil
}

func UpdateUserData(username string, nickname string, profilePictureUrl string) error {
	errMsg := myRpc.UpdateUserData(username, nickname, profilePictureUrl)

	if errMsg != "" {
		log.Println("get user data error: ", errMsg)
		return errors.New(errMsg)
	}
	return nil
}
