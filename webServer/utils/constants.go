package utils

const (
	OperaType_login      = "login"
	OperaType_logout     = "logout"
	OperaType_getData    = "get data"
	OperaType_updateData = "update data"
)
