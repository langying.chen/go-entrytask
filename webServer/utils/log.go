package utils

import (
	"log"
	"time"
)

func LogPrint(msg string) {
	getTime := time.Now()
	log.Println(msg, time.Since(getTime))
}
