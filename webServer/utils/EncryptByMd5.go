package utils

import (
	"crypto/md5"
	"encoding/hex"
)

func EncryptByMd5(src string) (hashString string) {
	h := md5.New()
	h.Write([]byte(src))
	hashString = hex.EncodeToString(h.Sum(nil))
	return hashString
}
