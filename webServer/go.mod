module webServer

go 1.12

require (
	github.com/astaxie/beego v1.12.3
	github.com/fatih/pool v3.0.0+incompatible
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
)
