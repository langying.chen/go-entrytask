package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
	"webServer/test"
)

//并发测试。并发1000个用户
func main() {
	runtime.GOMAXPROCS(6) //分配n个线程？用于测试，另外12-n个线程用于webServer和tcpServer。这个值会影响测试结果，是调优的一部分

	userCount := 100 //m协程用于测试，即并发量为m

	var wg sync.WaitGroup
	wg.Add(userCount)

	fmt.Println("start users")
	startTime := time.Now()
	for i := 0; i < userCount; i++ {
		go func() {
			defer wg.Done()

			test.RandomUserTest() //每个协程都去执行基准测试，基准测试中随机了1个用户，每个用户发起了10次请求
			err := recover()
			if err != nil {
				fmt.Println(err)
			}
		}()

		if (i+1)%100 == 0 || i+1 == userCount {
			fmt.Println("己启动", i+1, "个用户")
		}
	}

	fmt.Println("wait for users end up ")
	wg.Wait()

	duration := time.Since(startTime)
	fmt.Println("duration ", duration.Seconds(), " s")
	fmt.Println("qps", int64(userCount)*10/int64(duration.Seconds())) //QPS

}
