package myRpc

import "webServer/model"

// UserCheck 校验请求结构体
type UserCheck struct {
	Username string
	Token    string
}

// CheckResult 校验响应结构体
type CheckResult struct {
	IsChecked bool
	ErrMsg    string
}

// UserLogin 登陆请求结构体
type UserLogin struct {
	Username string
	Password string
}

// LoginResults 登陆响应结构体
type LoginResults struct {
	Token  string
	ErrMsg string
}

// UserLogout 登出请求结构体
type UserLogout struct {
	UserToken model.UserToken
}

// LogoutResults 登出响应结构体
type LogoutResults struct {
	ErrMsg string
}

// UserGetData 获取用户数据请求结构体
type UserGetData struct {
	Username string
}

// GetDataResults 获取用户数据响应结构体
type GetDataResults struct {
	UserData model.UserData
	ErrMsg   string
}

// UserUpdateData 更新用户数据请求结构体
type UserUpdateData struct {
	UserData model.UserData
}

// UpdateDataResults 获取用户数据响应结构体
type UpdateDataResults struct {
	ErrMsg string
}
