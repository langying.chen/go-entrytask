package myRpc

import (
	"github.com/astaxie/beego"
	"log"
	"net/rpc/jsonrpc"
	"webServer/model"
)

/*
	rpc解耦层
*/

// CheckLogin 校验rpc
func CheckLogin(username string, token string) (checked bool, errMsg string) {
	//连接rpc
	Conn, err1 := jsonrpc.Dial("tcp", beego.AppConfig.String("rpchost"))
	if err1 != nil {
		log.Fatalln("dialing error: ", err1) //println
	}

	//创建请求体和响应体
	req := UserCheck{username, token}
	var res CheckResult

	err := Conn.Call("CheckLoginRpc.CheckLogin", req, &res)
	if err != nil {
		log.Fatalln("CheckLoginRpc.CheckLogin error:", err)
	}

	return res.IsChecked, res.ErrMsg
}

// Login 登陆rpc
func Login(username string, password string) (token string, errMsg string) {
	//连接rpc
	Conn, err := jsonrpc.Dial("tcp", beego.AppConfig.String("rpchost"))
	if err != nil {
		log.Fatalln("dialing error: ", err)
	}

	req := UserLogin{username, password}
	var res LoginResults

	err = Conn.Call("LoginRpc.Login", req, &res)
	if err != nil {
		log.Fatalln("LoginRpc.Login error:", err)
	}

	return res.Token, res.ErrMsg
}

// Logout 登出rpc
func Logout(username string, token string) (errMsg string) {
	//连接rpc
	Conn, err1 := jsonrpc.Dial("tcp", beego.AppConfig.String("rpchost"))
	if err1 != nil {
		log.Fatalln("dialing error: ", err1)
	}

	//创建请求体和响应体
	req := UserLogout{UserToken: model.UserToken{Username: username, Token: token}}
	var res LogoutResults

	err := Conn.Call("LogoutRpc.Logout", req, &res)
	if err != nil {
		log.Fatalln("LogoutRpc.Logout error:", err)
	}

	return res.ErrMsg
}

// GetUserData 获取用户数据rpc
func GetUserData(username string) (userData model.UserData, errMsg string) {
	//连接rpc
	Conn, err1 := jsonrpc.Dial("tcp", beego.AppConfig.String("rpchost")) //conn是不是可以复用
	if err1 != nil {
		log.Fatalln("dialing error: ", err1)
	}

	//创建请求体和响应体
	req := UserGetData{Username: username}
	var res GetDataResults

	var err = Conn.Call("GetUserDataRpc.GetUserData", req, &res) //conn是不是没关，关掉是不是占用端口套接
	if err != nil {
		log.Fatalln("GetUserDataRpc.GetUserData error:", err)
	}

	return res.UserData, res.ErrMsg
}

// UpdateUserData 更新用户数据rpc
func UpdateUserData(username string, nickname string, profilePictureUrl string) (errMsg string) {
	//连接rpc
	Conn, err1 := jsonrpc.Dial("tcp", beego.AppConfig.String("rpchost"))
	if err1 != nil {
		log.Fatalln("dialing error: ", err1)
	}

	//创建请求体和响应体
	req := UserUpdateData{model.UserData{Username: username, Nickname: nickname, ProfilePictureUrl: profilePictureUrl}}
	var res UpdateDataResults

	err := Conn.Call("UpdateUserDataRpc.UpdateUserData", req, &res)
	if err != nil {
		log.Fatalln("UpdateUserDataRpc.UpdateUserData error:", err)
	}

	return res.ErrMsg
}
