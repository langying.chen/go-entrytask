package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"webServer/model"
	"webServer/utils"
)

//RandomUserTest基准测试，只是随机10000个用户发请求，没有并发
func RandomUserTest() {
	randomInt := rand.Intn(10000)
	//randomInt := 1
	username := fmt.Sprintf("xiaoming%d", randomInt)
	password := username

	passowordMD5 := utils.EncryptByMd5(password)

	client := &http.Client{}

	req, err := http.NewRequest("POST", "http://localhost:7777/login",
		strings.NewReader("username="+username+"&password="+passowordMD5))
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("Connection", "close")

	resp, err := client.Do(req)

	if err != nil {
		fmt.Println(err.Error())
		return
	} else if resp.StatusCode != 200 {
		fmt.Println("/login", resp.StatusCode, username)
	}

	body, err := ioutil.ReadAll(resp.Body)
	// handle error
	if err != nil {
		fmt.Println(err.Error())
		return
	} else if len(body) == 0 {
		fmt.Println("/userlogin body is empty")
	}
	defer resp.Body.Close()

	result := model.Result{"test", model.UserData{}, ""}
	err = json.Unmarshal(body, &result)
	if err != nil {
		fmt.Println(err.Error())
		return
	} else if result.ErrMsg != "" {
		fmt.Println(result.ErrMsg, username)
		return
	}

	for i := 0; i < 9; i++ { //每个用户发10次请求 表示获取用户信息这个动作被使用了n次。
		req, err := http.NewRequest("GET", "http://localhost:7777/req/data", nil)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		req.Header.Add("Connection", "close")
		req.Header.Set("Cookie", "token="+result.Data.(map[string]interface{})["Token"].(string)+";username="+username)

		res, err := client.Do(req)
		if err != nil {
			fmt.Println(err.Error())
			return
		} else if resp.StatusCode != 200 {
			fmt.Println("/home/data", resp.StatusCode, username)
			return
		}
		defer res.Body.Close()
	}
}
