package main

import (
	"github.com/astaxie/beego"
	_ "net/http/pprof"
	_ "webServer/routers"
)

func main() {
	beego.Run()
}
