package main

import (
	"testing"
	"webServer/test"
)

// 性能测试。
func BenchmarUser(b *testing.B) {
	for i := 0; i < b.N; i++ {
		test.RandomUserTest()
	}
}

func TestUser(t *testing.T) {
	test.RandomUserTest()
}
