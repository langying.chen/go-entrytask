package model

type Result struct {
	OperaType string      `json:"type"`
	Data      interface{} `json:"data"`
	ErrMsg    string      `json:"errMsg"`
}

func SuccessData(operaType string, data interface{}) Result {
	var result Result
	result.OperaType = operaType
	result.Data = data
	result.ErrMsg = ""
	return result
}

func ErrorData(operaType string, err error) Result {
	var result Result
	result.OperaType = operaType
	result.ErrMsg = err.Error()
	result.Data = nil
	return result
}
