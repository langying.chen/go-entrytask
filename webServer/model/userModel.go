package model

type User struct {
	Username          string `orm:"column(username);pk"`
	Password          string
	Nickname          string
	ProfilePictureUrl string
}

type UserData struct {
	Username          string
	Nickname          string
	ProfilePictureUrl string
}

type UserToken struct {
	Username string
	Token    string
}
