package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"log"
	model "webServer/model"
	"webServer/service"
	"webServer/utils"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) System() {
	c.TplName = "login.html"
}

func (c *MainController) Profile() {
	c.TplName = "profile.html"
}

// HasLogin 用户登陆权限校验
var HasLogin = func(ctx *context.Context) {
	//获取请求中的账号和密码
	CookieUsername, _ := ctx.Request.Cookie("username")
	CookieToken, _ := ctx.Request.Cookie("token")

	//检查用户是否登陆
	checked, err := service.CheckLogin(CookieUsername.Value, CookieToken.Value)

	if err != nil || !checked {
		if err != nil {
			log.Println(err.Error())
		} else {
			log.Println("not login")
		}
		//校验不通过，跳转到登陆界面
		ctx.Redirect(401, "/login")
	}
}

func (c *MainController) Login() {
	username := c.GetString("username")
	password := c.GetString("password")

	token, err := service.Login(username, password)
	if err != nil {
		c.Data["json"] = model.ErrorData(utils.OperaType_login, err)
	} else {
		c.Data["json"] = model.SuccessData(utils.OperaType_login, model.UserToken{Username: username, Token: token})
	}
	c.ServeJSON()
}

func (c *MainController) Logout() {
	CookieUsername, _ := c.Ctx.Request.Cookie("username")
	CookieToken, _ := c.Ctx.Request.Cookie("token")

	err := service.Logout(CookieUsername.Value, CookieToken.Value)

	if err != nil {
		c.Data["json"] = model.ErrorData(utils.OperaType_logout, err)
	} else {
		c.Data["json"] = model.SuccessData(utils.OperaType_logout, model.UserToken{Username: CookieUsername.Value, Token: CookieToken.Value})
	}
	c.ServeJSON()
}

func (c *MainController) GetUserData() {
	CookieUsername, _ := c.Ctx.Request.Cookie("username")

	userData, err := service.GetUserData(CookieUsername.Value)

	if err != nil {
		c.Data["json"] = model.ErrorData(utils.OperaType_getData, err)
	} else {
		c.Data["json"] = model.SuccessData(utils.OperaType_getData, userData)
	}
	c.ServeJSON()

}

func (c *MainController) UpdateUserData() {
	CookieUsername, _ := c.Ctx.Request.Cookie("username")
	nickname := c.GetString("nickname")
	profilePictureUrl := c.GetString("profilePictureUrl")

	err := service.UpdateUserData(CookieUsername.Value, nickname, profilePictureUrl)

	if err != nil {
		c.Data["json"] = model.ErrorData(utils.OperaType_updateData, err)
	} else {
		c.Data["json"] = model.SuccessData(utils.OperaType_updateData, nil)
	}
	c.ServeJSON()
}
