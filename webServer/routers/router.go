package routers

import (
	"github.com/astaxie/beego"
	"webServer/controllers"
)

func init() {
	beego.SetStaticPath("/static", "views/static")

	beego.Router("/", &controllers.MainController{}, "get:System;post:Login")
	beego.Router("/login", &controllers.MainController{}, "get:System;post:Login")
	beego.Router("/logout", &controllers.MainController{}, "post:Logout")

	ns := beego.NewNamespace("/req",
		beego.NSBefore(controllers.HasLogin),
		beego.NSRouter("/profile", &controllers.MainController{}, "get:Profile"),
		beego.NSRouter("/data", &controllers.MainController{}, "get:GetUserData"),
		beego.NSRouter("/update", &controllers.MainController{}, "post:UpdateUserData"),
	)
	beego.AddNamespace(ns)

}
