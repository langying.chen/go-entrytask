package myOrm

import (
	"errors"
	"github.com/astaxie/beego/orm"
	"log"
	"tcpServer/model"
)

//用username从数据库中查询数据
func GetUserByName(username string) (model.User, error) {
	o := orm.NewOrm()
	if o != nil {
		log.Println("create orm")
	}
	//read从数据库中查询，返回错误信息。如果有匹配的数据就将user赋值。
	user := model.User{Username: username}
	var userPtr *model.User = &user
	err := o.Read(userPtr, "username")

	return user, err
}

func UpdateUser(userData model.UserData) (updateNum int64, err error) {
	o := orm.NewOrm()
	//user := model.User{Username: userData.Username, Nickname: userData.Nickname, ProfilePictureUrl: userData.ProfilePictureUrl}
	//
	//return o.Update(&user)

	sql := "UPDATE user SET "

	toUpdate := false
	if len(userData.Nickname) > 0 {
		sql += "nickname = '" + userData.Nickname + "', "
		toUpdate = true
	}
	if len(userData.ProfilePictureUrl) > 0 {
		sql += "profile_picture_url = '" + userData.ProfilePictureUrl + "', "
		toUpdate = true
	}

	if !toUpdate {
		// 没有字段需要更新
		return -1, errors.New("nothing to update")
	}

	sql = sql[:len(sql)-2]
	condition := " WHERE username = '" + userData.Username + "' "
	sql += condition

	exec, err := o.Raw(sql).Exec()
	if err != nil {
		return 0, err
	} else {
		return exec.RowsAffected()
	}
}
