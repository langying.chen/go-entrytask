package main

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"
	"tcpServer/model"
	"tcpServer/myRpc"
)

func main() {
	//初始化数据库
	initDB()

	//注册及开启rpc服务  https://zhuanlan.zhihu.com/p/143961275
	rpc.RegisterName("CheckLoginRpc", new(myRpc.CheckLoginRpc)) //注册CheckToken方法
	rpc.RegisterName("LoginRpc", new(myRpc.LoginRpc))
	rpc.RegisterName("LogoutRpc", new(myRpc.LogoutRpc))
	rpc.RegisterName("GetUserDataRpc", new(myRpc.GetUserDataRpc))
	rpc.RegisterName("UpdateUserDataRpc", new(myRpc.UpdateUserDataRpc))

	lis, err := net.Listen("tcp", beego.AppConfig.String("rpchost"))
	if err != nil {
		log.Fatalln("listenTCP error: ", err)
		return
	}
	fmt.Fprintf(os.Stdout, "%s", "start connection")

	for {
		conn, err := lis.Accept() // 接收客户端连接请求
		if err != nil {
			continue
		}

		go func(conn net.Conn) { // 并发处理客户端请求
			fmt.Fprintf(os.Stdout, "%s", "new client in coming\n")
			jsonrpc.ServeConn(conn)
		}(conn)
	}

}

//初始化数据库，用beego的orm
func initDB() {
	//设置数据库类型
	orm.RegisterDriver("mysql", orm.DRMySQL)

	//获取配置文件中关于mysql的信息
	user := beego.AppConfig.String("mysqluser")
	password := beego.AppConfig.String("mysqlpass")
	name := beego.AppConfig.String("mysqldb")
	url := beego.AppConfig.String("mysqlurls")
	port := beego.AppConfig.String("mysqlport")
	maxIdle := 2
	maxConn := 10

	//注册一个默认的数据库，作为orm的默认使用
	orm.RegisterDataBase(
		"default",
		"mysql",
		user+":"+password+"@tcp("+url+":"+port+")/"+name+"?charset=utf8mb4",
		maxIdle,
		maxConn)

	//如果使用 orm.QuerySeter 进行高级查询的话，这个是必须的。也可以同时注册多个model
	orm.RegisterModel(new(model.User))

	//显示错误信息，方便调试
	orm.Debug = true

}
