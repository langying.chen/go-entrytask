package myRpc

import (
	"github.com/gomodule/redigo/redis"
	"tcpServer/service"
)

/*
	rpc解耦层
*/

var redisClient *redis.Pool

//使用go自带的rpc发布服务的话得用这种格式

//校验登陆状态
type CheckLoginRpc struct {
}

func (checkToken CheckLoginRpc) CheckLogin(userCheck UserCheck, checked *CheckResult) error {
	//调用本地的CheckLogin方法
	checked.IsChecked, checked.ErrMsg = service.CheckLogin(userCheck.Username, userCheck.Token)

	return nil
}

//用户登陆
type LoginRpc struct {
}

func (loginRpc LoginRpc) Login(userLogin UserLogin, loginResults *LoginResults) error {
	//调用本地的Login方法
	loginResults.Token, loginResults.ErrMsg = service.Login(userLogin.Username, userLogin.Password)

	return nil
}

//用户登出
type LogoutRpc struct {
}

func (logoutRpc LogoutRpc) Logout(userLogout UserLogout, logoutResults *LogoutResults) error {
	//调用本地的Logout方法
	logoutResults.Error = service.Logout(userLogout.UserToken.Username, userLogout.UserToken.Token)

	return nil
}

//获取用户数据
type GetUserDataRpc struct {
}

func (getUserDataRpc *GetUserDataRpc) GetUserData(userGetData UserGetData, getDataResults *GetDataResults) error {
	//调用本地的GetUserData方法
	getDataResults.UserData, getDataResults.Error = service.GetUserData(userGetData.Username)

	return nil
}

//更新用户数据
type UpdateUserDataRpc struct {
}

func (updateUserDataRpc UpdateUserDataRpc) UpdateUserData(userUpdateData UserUpdateData, updateDataResults *UpdateDataResults) error {
	//调用本地的UpdateUserData方法
	updateDataResults.Error = service.UpdateUserData(userUpdateData.UserData.Username, userUpdateData.UserData.Nickname, userUpdateData.UserData.ProfilePictureUrl)

	return nil
}
