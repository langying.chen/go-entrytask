package service

import (
	"encoding/json"
	"errors"
	"github.com/astaxie/beego"
	"github.com/gomodule/redigo/redis"
	"log"
	"tcpServer/model"
	"tcpServer/myOrm"
	"tcpServer/utils"
	"time"
)

var redisClient *redis.Pool

func init() {
	////初始化redis连接池
	//redisClient = utils.RedisCoon()
	redisHost := beego.AppConfig.String("redishost")

	timeoutSecond := time.Duration(3000) * time.Second

	redisClient = &redis.Pool{
		MaxIdle:   10,
		MaxActive: 40,
		Wait:      true,
		Dial: func() (redis.Conn, error) {
			con, err := redis.Dial("tcp", redisHost,
				redis.DialConnectTimeout(timeoutSecond),
				redis.DialReadTimeout(timeoutSecond),
				redis.DialWriteTimeout(timeoutSecond))
			if err != nil {
				log.Println("Get Redis Connection Fail")
				return nil, err
			}
			return con, nil
		},
	}
}

//校验用户是否登陆
func CheckLogin(username string, token string) (checked bool, errMsg string) {
	rdb := redisClient.Get()
	defer rdb.Close()

	value, err1 := redis.String(rdb.Do("GET", "token:"+token)) //因为redis不能存放两个相同的key，所以前面加个token+来区别这个token
	if err1 != nil {
		log.Println("Get From Redis Fail", errMsg)
		return false, errMsg
	} else {
		if username == value {
			return true, ""
		} else {
			return false, "not login"
		}
	}
}

func Login(username string, password string) (token string, errMsg string) {
	//数据库查询用户密码，校验用户登陆账号
	user, err := myOrm.GetUserByName(username)
	if err != nil {
		return "", "get user error"
	}

	var p = utils.EncryptByMd5(user.Password)
	if p == password {
		//创建token
		token := utils.GenerateToken(username)

		//连接redis，存储token
		rdb := redisClient.Get()
		defer rdb.Close()
		if rdb.Err() != nil {
			return "", "connect redis error"
		}
		rdb.Do("SET", "token:"+token, username, "EX", beego.AppConfig.String("redisEX"))

		//存储用户信息，用redis加快获取信息的速度
		userData := model.UserData{user.Username, user.Nickname, user.ProfilePictureUrl}
		userDataJSON, _ := json.Marshal(userData) //序列化用户信息，然后存到redis中
		rdb.Do("SET", "user:"+username, userDataJSON, "EX", beego.AppConfig.String("redisEX"))

		return token, ""
	} else {
		return "", "password wrong"
	}

}

func Logout(username string, token string) (err error) {
	//连接redis，删除token
	rdb := redisClient.Get()
	defer rdb.Close()
	if rdb.Err() != nil {
		return errors.New("connect redis error")
	}
	rdb.Do("DEL", "token:"+token)
	rdb.Do("DEL", "user:"+username)
	return nil
}

func GetUserData(username string) (model.UserData, error) {
	//先从redis中查找数据
	rdb := redisClient.Get()
	defer rdb.Close()

	userBytes, err := redis.Bytes(rdb.Do("GET", "user:"+username))
	if err == nil && len(userBytes) > 0 {
		userData := model.UserData{}
		json.Unmarshal(userBytes, &userData)
		return userData, nil
	}

	//redis中查不到，到数据库中查询
	user, err := myOrm.GetUserByName(username)
	userData := model.UserData{user.Username, user.Nickname, user.ProfilePictureUrl}
	if err != nil {
		return model.UserData{}, err
	}

	//将数据写入redis，方便下次查询
	userDataJSON, _ := json.Marshal(userData)
	rdb.Do("SET", "user:"+userData.Username, userDataJSON, "EX", beego.AppConfig.String("redisEX"))

	return userData, nil
}

//多个update同时存在，数据可能不一致
func UpdateUserData(username string, nickname string, profilePictureUrl string) error {
	userData := model.UserData{Username: username, Nickname: nickname, ProfilePictureUrl: profilePictureUrl}
	updateNum, err := myOrm.UpdateUser(userData)
	if err != nil || updateNum != 1 {
		return err
	}

	//先删除，后创建redis中对应的数据（相当于修改）
	rdb := redisClient.Get()
	defer rdb.Close()

	rdb.Do("DEL", "user:"+username) //redis操作放前面更好
	userDataJSON, _ := json.Marshal(userData)
	rdb.Do("SET", "user:"+username, userDataJSON, "EX", beego.AppConfig.String("redisEX")) //数据可能不一致，直接del，不set了

	return nil
}
