package utils

import "time"

func GenerateToken(username string) (token string) {
	return EncryptByMd5(username + time.Now().String())
}
